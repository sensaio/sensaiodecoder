//
// *****************************************************************************
//  @company        : Edge Technologies
//  @date           : 2023
//  @author         : Kévin Di Méo
// *****************************************************************************
//
// SensaIo LoRaWAN Payload Codec
//
// ****************************************************************************
//
//Copyright (C) 2023 Edge Technologies
//
//Permission to use, copy, modify, and/or distribute this software for any
//purpose with or without fee is hereby granted, provided that the above
//copyright notice and this permission notice appear in all copies.
//
//THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
//WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
//MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
//ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
//WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
//OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
//CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ****************************************************************************
//

// Standard for Tektelic
function Decoder(bytes,port) {
    var input= {};
    input.bytes = bytes;
    var data_out = {};
    var sensorTypes = [ 'NONE', 'PRESSURE', 'RTD', 'MOTION', 'TCK'];

    // Explose input bytes to binary string
    var binary = '';
    for(var i = 0; i < input.bytes.length; i++) {
        binary = ('00000000' + (input.bytes[i] >>>0).toString(2)).slice(-8) + binary;
    }

    // Check Sensor Type
    var sensorType = parseInt(binary.slice(-8), 2);

    if(sensorType < sensorTypes.length) {
        var warnings = [];
        var errors = [];
        data_out.data = {};

        var messageTypes = [ 'FIRST', 'PERIODIC', 'ALERT', 'LOG'];
        data_out.data.sensorType = sensorTypes[sensorType];
        binary = binary.slice(0, -8);
        data_out.data.messageType = messageTypes[parseInt(binary.slice(-2),2)];
        binary = binary.slice(0, -2);
        data_out.data.batteryLevel = parseInt(binary.slice(-7),2);
        binary = binary.slice(0, -7);
        data_out.data.version = parseInt(binary.slice(-3),2);
        binary = binary.slice(0, -3);
        data_out.data.version = parseInt(binary.slice(-3),2) + '.' +data_out.data.version;
        binary = binary.slice(0, -3);
        data_out.data.version = parseInt(binary.slice(-3),2) + '.' +data_out.data.version;
        binary = binary.slice(0, -3);
        data_out.data.deviceTemperature = parseInt(binary.slice(-9),2);
        binary = binary.slice(0, -9);
        data_out.data.deviceShock = parseInt(binary.slice(-13),2);
        binary = binary.slice(0, -13);
        var flags = binary.slice(-8);
        if (flags == "00000000") {
            data_out.data.flags = "OK";
        } else {
            data_out.data.flags = "KO";
            var flag_text = [
                "FLAG_TO136_FAILURE",
                "FLAG_LORA_FAILURE",
                "FLAG_SENSOR_I2C_FAILURE",
                "FLAG_EEPROM_FAILURE",
                "FLAG_ACCEL_FAILURE",
                "FLAG_SENSOR_WARNING",
                "FLAG_SWAP_FAILURE",
                "FLAG_UNKNOWN"
            ];

            for (var i = 0; i < flags.length; i++) {
                if(flags.charAt(i) == "1") {
                    warnings.push(flag_text[7-i]);
                }
            }
        }
        binary = binary.slice(0, -8);

        if (data_out.data.messageType === 'PERIODIC' || data_out.data.messageType === 'ALERT' || data_out.data.messageType === 'FIRST') {
            data_out.errors = ""; // default
            data_out.bitLength = 0;
            if (data_out.data.sensorType === 'PRESSURE') {
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    data_out.data.pressure = float32 * sign;
                }
                else {
                    data_out.data.pressure = 0;
                }
                binary = binary.slice(0, -32);
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    data_out.data.temperature = float32 * sign;
                }
                else {
                    data_out.data.temperature = 0;
                }
                binary = binary.slice(0, -32);
                data_out.bitLength = 2*32;
            } else if (data_out.data.sensorType === 'RTD' || data_out.data.sensorType === 'TCK') {
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    data_out.data.temperature = float32 * sign;
                }
                else {
                    data_out.data.temperature = 0;
                }
                binary = binary.slice(0, -32);
                data_out.bitLength = 32;
            } else if (data_out.data.sensorType === 'MOTION') {
                data_out.data.motion_position = parseInt(binary.slice(-8),2);
                binary = binary.slice(0, -8);
                data_out.data.motion_time = parseInt(binary.slice(-8),2);
                binary = binary.slice(0, -8);
                data_out.bitLength = 16;
            } else {
                data_out.errors ="Unknown sensor type";
            }
            binary = binary.slice(0, (-1)*data_out.bitLength);
            if (data_out.errors !== "") {
                errors.push(data_out.errors);
            }
        } else if (data_out.data.messageType === 'LOG') {
            data_out.data.sensorData = [];
            while (binary !== '') {
                var d = {};
                var t = parseInt(binary.slice(-32),2);
                binary = binary.slice(0, -32);
                var date = new Date(t * 1000);
                d.UTCTime = date.toUTCString();

                d.errors = ""; // default
                d.data = {};
                d.bitLength = 0;
                if (data_out.data.sensorType === 'PRESSURE') {
                    var int = parseInt(binary.slice(-32), 2);
                    if (int > 0 || int < 0) {
                        var sign = (int >>> 31) ? -1 : 1;
                        var exp = (int >>> 23 & 0xff) - 127;
                        var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                        var float32 = 0;
                        for (var i = 0; i < mantissa.length; i += 1) {
                            float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                            exp--;
                        }
                        data_out.data.pressure = float32 * sign;
                    }
                    else {
                        data_out.data.pressure = 0;
                    }
                    binary = binary.slice(0, -32);
                    var int = parseInt(binary.slice(-32), 2);
                    if (int > 0 || int < 0) {
                        var sign = (int >>> 31) ? -1 : 1;
                        var exp = (int >>> 23 & 0xff) - 127;
                        var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                        var float32 = 0;
                        for (var i = 0; i < mantissa.length; i += 1) {
                            float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                            exp--;
                        }
                        data_out.data.temperature = float32 * sign;
                    }
                    else {
                        data_out.data.temperature = 0;
                    }
                    binary = binary.slice(0, -32);
                    d.bitLength = 2*32;
                } else if (data_out.data.sensorType === 'RTD' || data_out.data.sensorType === 'TCK') {
                    var int = parseInt(binary.slice(-32), 2);
                    if (int > 0 || int < 0) {
                        var sign = (int >>> 31) ? -1 : 1;
                        var exp = (int >>> 23 & 0xff) - 127;
                        var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                        var float32 = 0;
                        for (var i = 0; i < mantissa.length; i += 1) {
                            float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                            exp--;
                        }
                        data_out.data.temperature = float32 * sign;
                    }
                    else {
                        data_out.data.temperature = 0;
                    }
                    binary = binary.slice(0, -32);
                    d.bitLength = 32;
                } else if (data_out.data.sensorType === 'MOTION') {
                    d.data.motion_position = parseInt(binary.slice(-8),2);
                    binary = binary.slice(0, -8);
                    d.data.motion_time = parseInt(binary.slice(-8),2);
                    binary = binary.slice(0, -8);
                    d.bitLength = 16;
                } else {
                    d.errors ="Unknown sensor type";
                }
                binary = binary.slice(0, (-1)*d.bitLength);
                if (d.errors !== "") {
                    errors.push(d.errors)
                }
                data_out.data.sensorData.push(d)
            }
        } else {
            errors.push("Unknown messageType");
        }

        if(warnings.length) {
            data_out.warnings = warnings;
        }

        if(errors.length) {
            data_out.errors = errors;
        }
        return data_out;
    }
    else if (sensorType == 255) {
        var warnings = [];
        var errors = [];
        data_out.data = {};

        var configTypeLoRaWAN = 4;
        var configTypeAlarm = 5;
        var configTypeAlarmExtended = 7;
        data_out.data.messageType = "CONFIG";
        binary = binary.slice(0, -8);
        data_out.data.configType = parseInt(binary.slice(-8),2);
        binary = binary.slice(0, -8);
        data_out.data.status = '0x' + parseInt(binary.slice(-8),2).toString(16);
        binary = binary.slice(0, -8);
        if(data_out.data.configType === configTypeLoRaWAN) {
            if (input.bytes.length !== 14) {
                warnings.push("LoRaWAN: Incomplete payload");
            }
            data_out.data.configType = "LoRaWAN";
            data_out.data.txDataRate = parseInt(binary.slice(-8),2);
            binary = binary.slice(0, -8);
            data_out.data.txPower = parseInt(binary.slice(-8),2);
            binary = binary.slice(0, -8);
            data_out.data.txRetries = parseInt(binary.slice(-8),2);
            binary = binary.slice(0, -8);
            data_out.data.appPort = parseInt(binary.slice(-8),2);
            binary = binary.slice(0, -8);
            data_out.data.txPeriodicity = parseInt(binary.slice(-32),2);
            binary = binary.slice(0, -32);
            data_out.data.isTxConfirmed = parseInt(binary.slice(-8),2) === 0 ? false: true;
            binary = binary.slice(0, -8);
            data_out.data.adrEnable = parseInt(binary.slice(-8),2) === 0 ? false: true;
            binary = binary.slice(0, -8);
            data_out.data.publicNetworkEnable = parseInt(binary.slice(-8),2) === 0 ? false: true;
        } else if(data_out.data.configType === configTypeAlarm) {
            data_out.data.configType = "Alarm";
            if (input.bytes.length < (17+3) || ((input.bytes.length-3) % 17 !== 0)) {
                warnings.push("Alarm: Incomplete payload");
            }
            data_out.data.alarms = [];
            while (input.bytes.length>=17) {
                input.bytes.length = input.bytes.length - 17;
                var alarm = {};
                var highThreshold = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    highThreshold = float32 * sign;
                }
                else {
                    highThreshold = 0;
                }
                binary = binary.slice(0, -32);

                var highHysteresis = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    highHysteresis = float32 * sign;
                }
                else {
                    highHysteresis = 0;
                }
                binary = binary.slice(0, -32);

                var lowHysteresis = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    lowHysteresis = float32 * sign;
                }
                else {
                    lowHysteresis = 0;
                }
                binary = binary.slice(0, -32);

                var lowThreshold = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    lowThreshold = float32 * sign;
                }
                else {
                    lowThreshold = 0;
                }
                binary = binary.slice(0, -32);

                var isActive = parseInt(binary.slice(-8),2);
                binary = binary.slice(0, -8);
                if (isActive === 1) {
                    alarm.highIsActive = true;
                    alarm.lowIsActive = false;
                    alarm.highThreshold = highThreshold;
                    alarm.highHysteresis = highHysteresis;
                } else if (isActive === 2) {
                    alarm.highIsActive = false;
                    alarm.lowIsActive = true;
                    alarm.lowHysteresis = lowHysteresis;
                    alarm.lowThreshold = lowThreshold;
                } else if (isActive === 3) {
                    alarm.highIsActive = true;
                    alarm.highThreshold = highThreshold;
                    alarm.highHysteresis = highHysteresis;
                    alarm.lowIsActive = true;
                    alarm.lowHysteresis = lowHysteresis;
                    alarm.lowThreshold = lowThreshold;
                }
                data_out.data.alarms.push(alarm);
            }
        } else if(data_out.data.configType === configTypeAlarmExtended) {
            data_out.data.configType = "AlarmExtended";
            if (byteLength < (20+3) || ((byteLength-3) % 20 !== 0)) {
                warnings.push("AlarmExtended: Incomplete payload");
            }
            data_out.data.alarms = [];
            while (byteLength>=20) {
                byteLength = byteLength - 20;
                var alarm = {};
                var highThreshold = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    highThreshold = float32 * sign;
                }
                else {
                    highThreshold = 0;
                }
                binary = binary.slice(0, -32);

                var highHysteresis = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    highHysteresis = float32 * sign;
                }
                else {
                    highHysteresis = 0;
                }
                binary = binary.slice(0, -32);

                var lowHysteresis = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    lowHysteresis = float32 * sign;
                }
                else {
                    lowHysteresis = 0;
                }
                binary = binary.slice(0, -32);

                var lowThreshold = 0;
                var int = parseInt(binary.slice(-32), 2);
                if (int > 0 || int < 0) {
                    var sign = (int >>> 31) ? -1 : 1;
                    var exp = (int >>> 23 & 0xff) - 127;
                    var mantissa = ((int & 0x7fffff) + 0x800000).toString(2);
                    var float32 = 0;
                    for (var i = 0; i < mantissa.length; i += 1) {
                        float32 += parseInt(mantissa[i], 10) ? Math.pow(2, exp) : 0;
                        exp--;
                    }
                    lowThreshold = float32 * sign;
                }
                else {
                    lowThreshold = 0;
                }
                binary = binary.slice(0, -32);

                var isActive = parseInt(binary.slice(-1),2);
                binary = binary.slice(0, -1);
                if (isActive === 1) {
                    alarm.highIsActive = true;
                    alarm.highThreshold = highThreshold;
                    alarm.highHysteresis = highHysteresis;
                }
                isActive = parseInt(binary.slice(-1),2);
                binary = binary.slice(0, -1);
                if (isActive === 1) {
                    alarm.lowIsActive = true;
                    alarm.lowThreshold = lowThreshold;
                    alarm.lowHysteresis = lowHysteresis;
                }
                var isActiveVariation = parseInt(binary.slice(-1),2);
                binary = binary.slice(0, -1);

                binary = binary.slice(0, -1); // Padding bit

                var wakeupPeriodList = ['15s', '30s', '1m', '2m', '5m', '15m', '30m', '1h', '3h', '6h', '12h'];
                var wakeupPeriod = parseInt(binary.slice(-4),2);
                binary = binary.slice(0, -4);
                alarm.wakeup_period = wakeupPeriodList[wakeupPeriod];

                var variation = parseInt(binary.slice(-8),2);
                binary = binary.slice(0, -8);
                if (isActiveVariation === 1) {
                    alarm.variationIsActive = true;
                    alarm.variation = variation;
                }
                data_out.data.alarms.push(alarm);

                // Padding bytes
                binary = binary.slice(0, -8);
                binary = binary.slice(0, -8);
            }
        } else {
            errors.push("Unknown config type");
        }

        if(warnings.length) {
            data_out.warnings = warnings;
        }

        if(errors.length) {
            data_out.errors = errors;
        }
        return data_out;

    } else {
        data_out.warnings = ['Unknown sensor type'];
        return data_out;
    }

    return data_out; // discard errors & warnings
}
